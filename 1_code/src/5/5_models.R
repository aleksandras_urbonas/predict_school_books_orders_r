cat('# 6. Models:\n')

### load data
load('2_data/math_4.rData')
cat('data loaded.\n')
# d <- en;rm(en)
d <- math;rm(math)

head(d,10)
# keep only the spenders
length(d$spender==1)
d <- d[d$spender==1,]

# keep variables
x <- c(
  'exp1'
  , 'exp2'
  , 'exp3'
  , 'exp_sum'
  , 'is_exam'
)
d <- d[,x]
# set the response feature
d$y <- d$is_exam;d$is_exam <- NULL

cat('\n\n * Review correlation:\n')
ro <- cor(d[,which(names(d)!='y')])
corrplot(
  ro
  , main='Corrplot'
  , sub='(c) AU 2018'
  )
cat('Correlation: sum of expenses is related to exp1.\n\n')

# x <- c(
#   'exp1'
#   , 'exp2'
#   , 'exp3'
#   , 'exam'
# )
# d <- d[,x]
# # setup the response feature
# d$y <- d$exam;d$exam <- NULL
# str(d)

# Set the random seed for reproducibility.
set.seed(1235)


# Use caret to create a 70%/30% stratified split.
idx <-
  createDataPartition(
    d$y
    , times = 1
    , p = 0.7
    , list = F
  )


# Create train and test
train <- d[idx,]
test <- d[-idx,]

# Verify proportions.
round(prop.table(table(train$y)),2)
round(prop.table(table(test$y)),2)

## Setup CV ####

k <- 10
k_cycles <- 3
set.seed(1235)
cv.folds <-
  createMultiFolds(
    train$y
    , k = k
    , times = k_cycles
  )

cv.cntrl <-
  trainControl(
    method = "repeatedcv"
    , number = k
    , repeats = k_cycles
    , index = cv.folds
  )

# Our data frame is non-trivial in size. As such, CV runs will take quite a long time to run. To cut down on total execution time, use the doSNOW package to allow for multi-core training in parallel.
# WARNING - The following code is configured to run on machine with at least 8 logical cores. Alter code to suit your HW environment.

# Time the code execution
start.time <- Sys.time()

# Create a cluster to work on 4 logical cores.
cl <- makeCluster(4, type = "SOCK")
registerDoSNOW(cl)

rpart.cv.1 <-
  train(
    y ~ .
    , data = train
    , method = "rpart"
    , trControl = cv.cntrl
    , tuneLength = 7
  )

# Processing is done, stop cluster.
stopCluster(cl)

# Total time of execution on workstation was approximately 4 minutes.
(total.time <- Sys.time() - start.time)

# Check out our results.
rpart.cv.1

# Now we can make predictions on the test data set using our trained random forest.
preds <- predict(rpart.cv.1, test)

# Drill-in on results
confusionMatrix(preds, test$y)




#### Model 2: RF #####
summary(d)

# Create a cluster to work on 10 logical cores.
cl <- makeCluster(4, type = "SOCK")
registerDoSNOW(cl)

# Time the code execution
start.time <- Sys.time()

# Re-run the training process with the additional feature.

rf.cv.2 <-
  train(
    y ~ .
    , data = train
    , method = "rf"
    , trControl = cv.cntrl
    , tuneLength = 7
    , importance = T
  )

# Processing is done, stop cluster.
stopCluster(cl)

# Total time of execution on workstation was
(total.time <- Sys.time() - start.time)

# Check the results.
rf.cv.2

# save to file
save(
  rf.cv.2
, file='5_models/rf.cv.Rdata'
)
cat('model saved to file.\n')

# Drill-down on the results.
# confusionMatrix(train$y, rf.cv.2$finalModel$predicted)

# How important was the new feature?
randomForest::varImpPlot(rf.cv.2$finalModel)

png(
  file='3_figures/5_m_rf_varImp.png'
)
randomForest::varImpPlot(rf.cv.2$finalModel)
dev.off()

# Now we can make predictions on the test data set using our trained random forest.
preds <- predict(rf.cv.2, test)

# Drill-in on results
confusionMatrix(preds, test$y)
